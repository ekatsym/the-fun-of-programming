-- Binary Heap Tree
data (Ord a) => Tree a = Null | Fork Int a (Tree a) (Tree a)


-- Ordered Queue
isEmpty     :: Tree a -> Bool
minElem     :: Tree a -> a
deleteMin   :: Tree a -> Tree a
insert      :: a -> Tree a -> Tree a
merge       :: Tree a -> Tree a -> Tree a

isEmpty Null           = True
isEmpty (Fork n x a b) = False

minElem (Fork n x a b) = x

deleteMin (Fork n x a b) = merge a b

insert x a = merge (Fork 1 Null Null) a

merge a Null = a
merge Null b = b
merge a b =
  | minElem a <= minElem b = join a b
  | otherwise              = join b a

join (Fork n x a b) c = Fork (n + size c) x aa (merge bb cc)
    where (aa, bb, cc) = orderBySize a b c
